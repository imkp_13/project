import React from "react";
import Header from "../components/Header";
import styles from "./home.module.css";
import Carousel from "../components/Carousel";
import useDocumentDimensions from "../hooks/useDocumentDimension";
import Card from "../components/Card";

const Home = () => {
  const { width } = useDocumentDimensions();
  const images = [
    "https://images.pexels.com/photos/842711/pexels-photo-842711.jpeg?auto=compress&cs=tinysrgb&w=1440",
    "https://images.pexels.com/photos/1402787/pexels-photo-1402787.jpeg?auto=compress&cs=tinysrgb&w=1440",
    "https://images.pexels.com/photos/1148998/pexels-photo-1148998.jpeg?auto=compress&cs=tinysrgb&w=1440",
  ];

  const cards = [
    {
      title: "Card 1",
      description: "Description for Card 1",
      imageUrl: "https://via.placeholder.com/300", // Replace with actual image URL
    },
    {
      title: "Card 2",
      description: "Description for Card 2",
      imageUrl: "https://via.placeholder.com/300", // Replace with actual image URL
    },
    {
      title: "Card 3",
      description: "Description for Card 3",
      imageUrl: "https://via.placeholder.com/300", // Replace with actual image URL
    },
    {
      title: "Card 4",
      description: "Description for Card 4",
      imageUrl: "https://via.placeholder.com/300", // Replace with actual image URL
    },

    // Add more cards as needed
  ];
  return (
    <>
      <Header />
      <div
        className={
          width > 450 ? styles.banner_wrapper : styles.banner_wrapper_mobile
        }
      >
        <Carousel images={images} />
        <div className={styles.text_center}>
          <h1>Explain how thing works</h1>
          <p>Help users decide what to do next. Some examples below.</p>
        </div>
        <div
          className={cards.length > 3 ? styles.card_list : styles.card_list_3}
        >
          {cards.map((card, index) => (
            <Card
              key={index}
              title={card.title}
              description={card.description}
              imageUrl={card.imageUrl}
            />
          ))}
        </div>
      </div>
    </>
  );
};

export default Home;
