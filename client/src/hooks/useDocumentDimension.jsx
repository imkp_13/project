import { useState, useEffect } from "react";

const useDocumentDimensions = () => {
  const [dimensions, setDimensions] = useState({
    height: window.document.documentElement.clientHeight,
    width: window.document.documentElement.clientWidth,
  });

  useEffect(() => {
    const handleResize = () => {
      setDimensions({
        height: window.document.documentElement.clientHeight,
        width: window.document.documentElement.clientWidth,
      });
    };

    window.addEventListener("resize", handleResize);

    // Clean up event listener on component unmount
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []); // Empty dependency array ensures effect runs only once

  return dimensions;
};

export default useDocumentDimensions;
