// OffCanvasMenu.jsx

import React from "react";
import styles from "./offcanvas.module.css";

const OffCanvas = ({ showMenu, children }) => {
  return (
    <div
      className={styles.off_canvas_menu + (showMenu ? ` ${styles.open}` : "")}
    >
      {children}
    </div>
  );
};

export default OffCanvas;
