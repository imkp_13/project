// Carousel.js

import React, { useEffect, useState } from "react";
import styles from "./carousel.module.css";
import { FaArrowLeft, FaArrowRight } from "react-icons/fa";

const Carousel = ({ images }) => {
  const [currentSlide, setCurrentSlide] = useState(0);

  useEffect(() => {
    const interval = setInterval(() => {
      nextSlide();
    }, 3000); // Change slide every 3 seconds (adjust as needed)

    return () => {
      clearInterval(interval); // Clear interval on component unmount
    };
  }, [currentSlide]); // Restart interval whenever currentSlide changes

  const nextSlide = () => {
    const next = currentSlide + 1 >= images.length ? 0 : currentSlide + 1;
    setCurrentSlide(next);
  };

  const prevSlide = () => {
    const prev = currentSlide - 1 < 0 ? images.length - 1 : currentSlide - 1;
    setCurrentSlide(prev);
  };

  return (
    <div className={styles.carousel}>
      {/* <button
        className={`${styles.carousel_control} ${styles.prev}`}
        onClick={prevSlide}
      >
        <FaArrowLeft />
      </button> */}
      <img
        className={styles.carousel_image}
        src={images[currentSlide]}
        alt="Carousel Slide"
      />
      <div className={styles.text_overlay}>
        <h1>This is your marketplace.</h1>
        <p>
          You're on the landing page. Use this page to communicate your value
          proposition.
          <br /> You can edit this page in Console by going to Content -&gt;
          Pages -&gt; landing-page.
        </p>
        <button className={styles.browse_listing}>Browse Listing</button>
      </div>
      {/* <button
        className={`${styles.carousel_control} ${styles.next}`}
        onClick={nextSlide}
      >
        <FaArrowRight />
      </button> */}
    </div>
  );
};

export default Carousel;
