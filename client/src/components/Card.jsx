import React from "react";
import styles from "./card.module.css"; // Import CSS Modules for styling
import useDocumentDimensions from "../hooks/useDocumentDimension";

const Card = ({ title, description, imageUrl }) => {
  const { width } = useDocumentDimensions();

  return (
    <div
      className={
        width < 600
          ? styles.card_mobile
          : width >= 600 && width < 900
          ? styles.card_tab
          : styles.card
      }
    >
      <img className={styles.card_image} src={imageUrl} alt={title} />
      <div className={styles.card_content}>
        <h3 className={styles.card_title}>{title}</h3>
        <p className={styles.card_description}>{description}</p>
      </div>
    </div>
  );
};

export default Card;
