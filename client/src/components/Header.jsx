import React, { useState } from "react";
import { FaBars, FaTimes } from "react-icons/fa";
import useDocumentDimensions from "../hooks/useDocumentDimension";
import styles from "./header.module.css";
import OffCanvas from "./OffCanvas";

const Header = () => {
  const { width } = useDocumentDimensions();
  const [showMenu, setShowMenu] = useState(false);

  return (
    <div
      className={width > 450 ? styles.header_main : styles.header_main_mobile}
    >
      <div className={styles.logo_container}>
        <img
          src="https://sharetribe-assets.imgix.net/664f22b3-ecc2-43f6-b39d-6d168c7fc35f/raw/39/a0d3d875b64248d1394818264c71a9eb318e16?auto=format&fit=clip&h=48&w=370&s=2b86c02f1188d2f7d6cdef67938e34bd"
          alt=""
          className={styles.logo}
        />
      </div>
      {width < 900 ? (
        <>
          {!showMenu && (
            <div
              onClick={() => {
                setShowMenu(true);
              }}
              className={styles.menu_bar}
            >
              <FaBars />
            </div>
          )}
        </>
      ) : (
        <div className={styles.menu_container}>
          <ul className={styles.menu_list}>
            <li className={styles.menu_items}>Home</li>
            <li className={styles.menu_items}>How it works</li>
            <li className={styles.menu_items}>Services</li>
            <li className={styles.menu_items}>Blogs</li>
          </ul>
          <div className={styles.button_container}>
            <button className={styles.join_btn}>Join Now</button>
          </div>
        </div>
      )}
      {showMenu && width < 900 && (
        <OffCanvas showMenu={showMenu}>
          <>
            <div className={styles.top_container}>
              <img
                src="https://sharetribe-assets.imgix.net/664f22b3-ecc2-43f6-b39d-6d168c7fc35f/raw/39/a0d3d875b64248d1394818264c71a9eb318e16?auto=format&fit=clip&h=48&w=370&s=2b86c02f1188d2f7d6cdef67938e34bd"
                alt=""
                className={styles.logo}
              />
              <div
                onClick={() => {
                  setShowMenu(false);
                }}
                className={styles.cross_icon}
              >
                <FaTimes />
              </div>
            </div>
            <nav className={styles.menu}>
              <ul>
                <li>Home</li>
                <li>How it works</li>
                <li>Services</li>
                <li>Blogs</li>
              </ul>
              <button className={styles.join_btn}>Join Now</button>
            </nav>
          </>
        </OffCanvas>
      )}
    </div>
  );
};

export default Header;
